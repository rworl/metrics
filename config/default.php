<?php

return [
    'internal' => [
        'host' => 'https://api.ripehq.com',
        'path_prefix' => 'rwmetrics'
    ],
    'flush_seconds' => 15
];