<?php

namespace RWMetric\Collectors\Route;

use Illuminate\Routing\Events\RouteMatched;

class Listener
{
    const UnknownKey = 'Unknown';

    public function __construct($tracer, $events)
    {
        $this->tracer = $tracer;
        $this->events = $events;
    }

    public function listen()
    {
        $this->events->listen(RouteMatched::class, function(RouteMatched $event) {
            $route = $event->route;

            $this->tracer->meta('route',  self::UnknownKey);

            if (!is_a($route, \Illuminate\Routing\Route::class)) {
                return;
            }

            $action = $route->getAction();

            if ($this->isControllerAction($action)) {
                $this->tracer->meta('route', $this->formatControllerAction($action));
            } else if ($this->isClosureAction($action)) {
                $this->tracer->meta('route', $this->formatClosureAction($route));
            }
        });
    }

    private function isControllerAction($action)
    {
        return isset($action['controller']) && strpos($action['controller'], '@') !== false && isset($action['namespace']);
    }

    private function isClosureAction($action)
    {
        return isset($action['uses']) && $action['uses'] instanceof \Closure;
    }

    private function formatControllerAction($action)
    {
        $namespace = $action['namespace'] . "\\";
        return str_replace($namespace, '', $action['controller']);
    }

    private function formatClosureAction($route)
    {
        return 'Closure@' . $route->uri();
    }
}