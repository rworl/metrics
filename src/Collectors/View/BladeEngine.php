<?php

namespace RWMetric\Collectors\View;

use RWMetric\MetricFactory;
use Illuminate\Contracts\View\Engine;

class BladeEngine implements Engine
{
    public function __construct(MetricFactory $tracer, $engine)
    {
        $this->tracer = $tracer;
        $this->engine = $engine;
        $this->resourcePath = resource_path('views').'/';
    }

    public function __call($method, $parameters)
    {
        return $this->engine->{$method}(... $parameters);
    }

    public function get($path, array $data = [])
    {
        return $this->tracer->execute(function() use ($path, $data) {
            return $this->engine->get($path, $data);
        }, $this->normalizeName($path), '', 'view');
    }

    private function normalizeName($path)
    {
        return str_replace($this->resourcePath, '', $path);
    }
}