<?php

namespace RWMetric\Collectors\Database;

use Illuminate\Database\DatabaseManager;

class Listener
{
    public function __construct(DatabaseManager $database, $tracer)
    {
        $this->tracer = $tracer;
        $this->database = $database;
    }

    public function listen()
    {
        $this->database->listen(function($query) {
            $start = round(microtime(true) * 1000, 1) - $query->time;
            $this->tracer->record($query->sql, 'db', round($start, 1), round($query->time, 1));
        });
    }
}