<?php

namespace RWMetric\Collectors\Request;

use Illuminate\Foundation\Http\Events\RequestHandled;

class Listener
{
    public function __construct($events, $tracer)
    {
        $this->tracer = $tracer;
        $this->events = $events;
        $this->start = microtime(true) * 1000;
    }

    public function listen()
    {
        $this->events->listen(RequestHandled::class, function(RequestHandled $event) {
            $status = $event->response->status();
            $contentLength = strlen($event->response->content());
            $duration = round(microtime(true) * 1000 - $this->start, 1);

            $this->tracer->meta('request_duration', $duration);
            $this->tracer->meta('http_status', $status);
            $this->tracer->meta('content_length', $contentLength);

            $this->tracer->flush();
        });
    }
}