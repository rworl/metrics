<?php

namespace RWMetric\Collectors\Redis;

use RWMetric\MetricFactory;

class RedisManager
{
    const NullNameKey = '__rwmetric_null';

    protected $wrappers = [];

    public function __construct($originalRedisManager, MetricFactory $tracer)
    {
        $this->redisManager = $originalRedisManager;
        $this->tracer = $tracer;
    }

    public function connection($name = null)
    {
        $lookupKey = $name;
        if ($name === null) {
            $lookupKey = self::NullNameKey;
        }

        if ($name === null || !array_key_exists($lookupKey, $this->wrappers)) {
            $connection = $this->redisManager->connection($name);
            $this->wrappers[$lookupKey] = new RedisConnection($connection, $this->tracer);
        }

        return $this->wrappers[$lookupKey];
    }

    public function __call($method, $parameters)
    {
        return $this->connection()->{$method}(...$parameters);
    }
}

