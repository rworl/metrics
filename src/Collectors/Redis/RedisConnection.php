<?php

namespace RWMetric\Collectors\Redis;

use Illuminate\Redis\Connections\Connection;
use RWMetric\MetricFactory;

class RedisConnection 
{
    public function __construct($originalConnection, MetricFactory $tracer)
    {
        $this->connection = $originalConnection;
        $this->tracer = $tracer;
    }

    public function __call($method, $parameters)
    {
        return $this->tracer->execute(function() use ($method, $parameters) {
            return $this->connection->{$method}(...$parameters);
        }, $method, 'redis');
    }
}