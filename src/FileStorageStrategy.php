<?php

namespace RWMetric;

class FileStorageStrategy
{
    const FORMAT = '.jsonl';

    public static function getBucketDirectory()
    {
        return storage_path(config('rwmetric.internal.path_prefix'));
    }

    public static function getBucketFilePath($time)
    {
        $pid = getmypid();
        $prefix = config('rwmetric.internal.path_prefix');
        $length = config('rwmetric.flush_seconds');

        $bucketedTimestamp = round($time / $length) * $length;

        $directory = "$prefix/$bucketedTimestamp";

        $directory = storage_path($directory);

        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }

        $name = $pid.self::FORMAT;
        $filePath = $directory."/".$name;

        return $filePath;
    }
}