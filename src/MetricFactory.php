<?php

namespace RWMetric;

use Ramsey\Uuid\UuidFactory;

class MetricFactory
{
    protected $trace;

    protected $writer;

    protected $spans = [];

    protected $meta = [];

    protected $spanCounter = 0;

    protected $spanBaseTimestamp = 0;

    protected $traceId;

    public function __construct(UuidFactory $uuidFactory, $writer, $reader)
    {
        $this->traceId = $uuidFactory->uuid4();
        $this->spanBaseTimestamp = microtime(true);
        $this->writer = $writer;
        $this->reader = $reader;
    }

    public function meta($key, $value = null)
    {
        if ($key && $value) {
            $this->meta[$key] = $value;
        } else if ($key) {
            if (array_key_exists($key, $this->meta)) {
                return $this->meta[$key];
            }
        }
    }

    public function record($operation, $category, $timestamp, $duration)
    {
        $spanId = $this->spanCounter++;

        $relativeTimestamp = $timestamp - $this->spanBaseTimestamp;

        $this->spans[] = [
            'operation' => $operation,
            'category' => $category,
            'timestamp' => $relativeTimestamp,
            'duration' => $duration,
            'index' => $spanId,
        ];
    }

    public function execute($closure, $operation, $category)
    {
        $start = microtime(true) * 1000;

        if (count($this->spans) == 0 && $this->spanBaseTimestamp == 0) {
            $this->spanBaseTimestamp = $start;
        }

        $value = $closure();
        $end = microtime(true) * 1000;

        $this->record($operation, $category, round($start, 1), round($end - $start, 1));

        return $value;
    }

    public function flush()
    {
        $this->roundTimestamps();
        $this->aggregateCategories();

        $trace = [
            'spans' => $this->spans,
            'meta' => $this->meta,
            'timestamp' => $this->spanBaseTimestamp,
            'id' => $this->traceId
        ];

        $this->submitTrace($trace);

        $this->spans = [];
        $this->meta = [];
        $this->spanCounter = 0;
    }

    protected function roundTimestamps()
    {
        $this->spans = array_map(function($item) {
            $item['duration'] = round($item['duration'], 1);
            $item['timestamp'] = round($item['timestamp'], 1);
            return $item;
        }, $this->spans);

        $this->spanBaseTimestamp = round($this->spanBaseTimestamp);
    }

    protected function aggregateCategories()
    {
        $aggregateCategories = ['db', 'redis'];

        $aggregate = array_reduce($this->spans, function($carry, $span) use ($aggregateCategories) {
            if (!in_array($span['category'], $aggregateCategories)) {
                return $carry;
            }

            if (!isset($carry[$span['category'].'_count'])) {
                $carry[$span['category'].'_count'] = 0;
                $carry[$span['category'].'_duration'] = 0;
            }

            $carry[$span['category'].'_count'] = $carry[$span['category'].'_count'] + 1;
            $carry[$span['category'].'_duration'] = $carry[$span['category'].'_count'] + $span['duration'];

            return $carry;
        }, []);

        foreach($aggregate as $key => $value) {
            $this->meta($key, $value);
        }
    }

    protected function submitTrace($trace)
    {
        $data = json_encode($trace);

        $this->writer->write($data);
    }

}