<?php

namespace RWMetric\Writer;

use RWMetric\FileStorageStrategy;

class Writer
{
    public function write($json)
    {
        $data = $json."\n";

        file_put_contents(FileStorageStrategy::getBucketFilePath(time()), $data, FILE_APPEND | LOCK_EX);
    }
}