<?php

namespace RWMetric\Providers;

use RWMetric\MetricFactory;
use Illuminate\Support\ServiceProvider;

class MetricServiceProvider extends ServiceProvider
{
    const ConfigPath = '/../../config/rwmetric.php';
    const DefaultConfigPath = '/../../config/default.php';

    const SingletonKey = 'rwmetric';
    const Writer = 'rwmetricwriter';
    const Reader = 'rwmetricreader';

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \RWMetric\Console\Commands\SendEventBatch::class
            ]);
        }

        $this->publishes([
            __DIR__.self::ConfigPath => config_path('rwmetric.php')
        ]);

        $this->registerDatabaseListener();
        $this->registerViewProxy();
        $this->registerRedisProxy();
        $this->registerResponseListener();
        $this->registerRouteListener();
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.self::DefaultConfigPath, 'rwmetric');

        $this->app->singleton(self::SingletonKey, function($app) {
            return new \RWMetric\MetricFactory(
                $app->make(\Ramsey\Uuid\UuidFactory::class), $this->writer(), $this->reader()
            );
        });

        $this->app->singleton(self::Writer, function($app) {
            return new \RWMetric\Writer\Writer();
        });

        $this->app->singleton(self::Reader, function($app) {
            return new \RWMetric\Reader\Reader();
        });
    }

    public function reader()
    {
        return $this->app[self::Reader];
    }

    public function writer()
    {
        return $this->app[self::Writer];
    }

    private function tracer()
    {
        return $this->app[self::SingletonKey];
    }

    private function dispatcher()
    {
        return $this->app['events'];
    }

    private function database()
    {
        return $this->app['db'];
    }

    private function router()
    {
        return $this->app['router'];
    }

    private function registerResponseListener()
    {
        $listener = new \RWMetric\Collectors\Request\Listener($this->dispatcher(), $this->tracer());
        $listener->listen();
    }

    private function registerDatabaseListener()
    {
        $listener = new \RWMetric\Collectors\Database\Listener($this->database(), $this->tracer());
        $listener->listen();
    }

    private function registerViewProxy()
    {
        $this->app->extend('view.engine.resolver', function($resolver) {
            $original = $resolver->resolve('blade');
            $resolver->register('blade', function() use ($original) {
                return new \RWMetric\Collectors\View\BladeEngine($this->tracer(), $original);
            });
            
            return $resolver;
        });
    }

    private function registerRedisProxy()
    {
        $this->app->extend('redis', function($redis) {
            return new \RWMetric\Collectors\Redis\RedisManager($redis, $this->tracer());
        });
    }

    private function registerRouteListener()
    {
        $listener = new \RWMetric\Collectors\Route\Listener($this->tracer(), $this->dispatcher());
        $listener->listen();
    }
}