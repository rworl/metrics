<?php

namespace RWMetric\Reader;

use RWMetric\FileStorageStrategy;

class Reader
{
    public function read($currentTime)
    {
        $readUntilTime = $currentTime - $this->flushSeconds();

        $files = $this->getBucketFiles($readUntilTime);

        $jsonl = $this->gatherFileData($files);

        return $jsonl;
    }

    public function cleanUpBuckets($currentTime)
    {
        $readUntilTime = $currentTime - $this->flushSeconds();
        $files = $this->getBucketFiles($readUntilTime);
        $directories = $this->getBucketDirectories($readUntilTime);

        foreach($files as $file) {
            unlink($file);
        }

        foreach($directories as $directory) {
            rmdir($directory);
        }
    }

    private function flushSeconds()
    {
        return config('rwmteric.flush_seconds');
    }

    private function gatherFileData($files)
    {
        $chunks = [];
        
        foreach($files as $file) {
            $data = file_get_contents($file);
            if ($data !== false) {
                $chunks[] = $data;
            }
        }

        return implode($chunks);
    }

    private function getBucketDirectories($readUntilTime)
    {
        $prefix = FileStorageStrategy::getBucketDirectory();

        $directories = array_filter(glob("$prefix/*"), function($item) use ($readUntilTime) {
            $elements = explode("/", $item);
            $name = array_pop($elements);

            return is_dir($item) && is_numeric($name) && $readUntilTime >= (int)$name;
        });

        return $directories;
    }

    private function getBucketFiles($readUntilTime)
    {
        $directories = $this->getBucketDirectories($readUntilTime);

        $listings = array_map(function($directory) {
            return array_filter(glob($directory."/*".FileStorageStrategy::FORMAT), function($item) {
                return !is_dir($item);
            });
        }, $directories);

        $files = [];
        foreach($listings as $fileArray) {
            if(!is_array($fileArray)) {
                continue;
            }

            foreach($fileArray as $file) {
                $files[] = $file;
            }
        }

        return $files;
    }
}