<?php

namespace RWMetric\Console\Commands;

use RWMetric\Reader\Reader;
use Illuminate\Console\Command;

class SendEventBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rwmetric:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send queued events';

    /**
     * Event queue reader.
     *
     * @var Reader
     */
    protected $reader;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(Reader $reader)
    {
        parent::__construct();

        $this->reader = $reader;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = time();

        $data = $this->reader->read($time);

        $success = $this->postData($data);
        if ($success) {
            $this->reader->cleanUpBuckets($time);
        }
    }

    private function postData($data)
    {        
        $gzipData = gzencode($data);
        
        $apiHost = config('rwmetric.internal.host');
        $apiKey = config('rwmetric.api_key');
        
        $request = curl_init("$apiHost/v1/events");
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($request, CURLOPT_POSTFIELDS, $gzipData);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            'Content-Encoding: gzip',
            'Content-Type: application/x-ndjson',
            'Content-Length: ' . strlen($gzipData),
            "X-Auth-Token: $apiKey"
        ]);

        $result = curl_exec($request);

        $responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);

        curl_close($request);

        return $responseCode >= 200 && $responseCode < 400;
    }
}